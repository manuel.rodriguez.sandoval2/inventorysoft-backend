package inventory.soft.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import inventory.soft.entity.TipoProducto;
import inventory.soft.repository.TipoProductoRepository;

@Service
public class TipoProductoServiceImpl implements TipoProductoService {
	
	
	
	@Autowired
	private TipoProductoRepository repository;
	@Transactional(readOnly=true)
	@Override
	public Iterable<TipoProducto> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return repository.findAll(pageable);
	}
	


	@Override
	@Transactional(readOnly=true)
	public Optional<TipoProducto> findbyId(Long id) {
		// TODO Auto-generated method stub
		return repository.findById(id);
		
	}

	@Override
	@Transactional
	public TipoProducto save(TipoProducto tipoProducto) {
		// TODO Auto-generated method stub
		
		TipoProducto tipoProductoCod = this.repository.findTipoProductoById(tipoProducto.getIdTipoProducto());
		
		if (tipoProductoCod==null) {
			return repository.save(tipoProducto);
			
		}
		return null;
		
	}

	@Override
	@Transactional
	public boolean deleteById(Long id) {

		Optional<TipoProducto> tipoProductoOP = this.repository.findById(id);

		if (tipoProductoOP.isPresent()) {
			repository.deleteById(id);
			return true;
		}
		return false;

	}

}


	
	

	





