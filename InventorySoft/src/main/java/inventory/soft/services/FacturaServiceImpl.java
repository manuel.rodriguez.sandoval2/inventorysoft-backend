package inventory.soft.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import inventory.soft.entity.Factura;
import inventory.soft.repository.FacturaRepository;



@Service
public class FacturaServiceImpl implements FacturaService {

	@Autowired
	private FacturaRepository repository;
	@Transactional(readOnly=true)
	@Override
	public Iterable<Factura> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return repository.findAll(pageable);
	}
	


	@Override
	@Transactional(readOnly=true)
	public Optional<Factura> findbyId(Long id) {
		// TODO Auto-generated method stub
		return repository.findById(id);
		
	}

	@Override
	@Transactional
	public Factura save(Factura factura) {
		// TODO Auto-generated method stub
		
		Factura facturaCod = this.repository.findFacturaById(factura.getId());
		
		if (facturaCod==null) {
			return repository.save(factura);
			
		}
		return null;
		
	}

	@Override
	@Transactional
	public boolean deleteById(Long id) {

		Optional<Factura> facturaOP = this.repository.findById(id);

		if (facturaOP.isPresent()) {
			repository.deleteById(id);
			return true;
		}
		return false;

	}

}


	
	

	







