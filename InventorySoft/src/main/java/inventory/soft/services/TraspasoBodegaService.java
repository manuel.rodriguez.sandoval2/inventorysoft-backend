package inventory.soft.services;

import java.util.Optional;

import org.springframework.data.domain.Pageable;

import inventory.soft.entity.Ingreso;
import inventory.soft.entity.TraspasoBodega;

public interface TraspasoBodegaService {

	public Iterable<TraspasoBodega> findAll(Pageable pageable);

	public Optional<TraspasoBodega> findbyId(Long id);

	public TraspasoBodega save(TraspasoBodega traspasoBodega);

	public boolean deleteById(Long id);
	
	//public TraspasoBodega update (Ingreso ingreso);

}
