package inventory.soft.services;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;

import inventory.soft.entity.Bodega;
import inventory.soft.entity.Usuario;



public interface UsuarioService {
	
	public Iterable<Usuario>findAll(Pageable pageable);
	
	public Optional<Usuario>findbyId(Long id);
	
//	public Optional<Usuario>findbyRut(String rut);
	
	public Usuario save(Usuario usuario);

	public boolean deleteById(Long id);
	
	public List<Usuario> findByNombre();

}
