package inventory.soft.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import inventory.soft.entity.Ingreso;
import inventory.soft.entity.TraspasoBodega;
import inventory.soft.repository.TraspasoBodegaRepository;

@Service
public class TraspasoBodegaServiceImpl implements TraspasoBodegaService {

	@Autowired
	private TraspasoBodegaRepository repository;
	@Transactional(readOnly=true)
	@Override
	public Iterable<TraspasoBodega> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return repository.findAll(pageable);
	}
	


	@Override
	@Transactional(readOnly=true)
	public Optional<TraspasoBodega> findbyId(Long id) {
		// TODO Auto-generated method stub
		return repository.findById(id);
		
	}

	@Override
	@Transactional
	public TraspasoBodega save(TraspasoBodega traspasoBodega) {
		// TODO Auto-generated method stub
		
		TraspasoBodega traspasoBodegaCod = this.repository.findTraspasoBodegaById(traspasoBodega.getId());
		
		if (traspasoBodegaCod==null) {
			return repository.save(traspasoBodega);
			
		}
		return null;
		
	}

	@Override
	@Transactional
	public boolean deleteById(Long id) {

		Optional<TraspasoBodega> traspasoBodegaOP = this.repository.findById(id);

		if (traspasoBodegaOP.isPresent()) {
			repository.deleteById(id);
			return true;
		}
		return false;

	}



	/*@Override
	public TraspasoBodega update(Ingreso ingreso) {
		// TODO Auto-generated method stub
		return repository.updateCantidad(ingreso);
	}*/

}