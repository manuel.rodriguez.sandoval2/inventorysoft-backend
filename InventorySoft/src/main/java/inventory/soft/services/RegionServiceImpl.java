package inventory.soft.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import inventory.soft.entity.Region;
import inventory.soft.repository.RegionRepository;

@Service
public class RegionServiceImpl implements RegionService {

	@Autowired
	private RegionRepository repository;
	@Transactional(readOnly=true)
	@Override
	public Iterable<Region> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return repository.findAll(pageable);
	}
	
	

	

	@Override
	@Transactional(readOnly=true)
	public Optional<Region> findbyId(Long id) {
		// TODO Auto-generated method stub
		return repository.findById(id);
		
	}

	@Override
	@Transactional
	public Region save(Region region) {
		// TODO Auto-generated method stub
		
		Region regionCod = this.repository.findRegionById(region.getId());
		
		if (regionCod==null) {
			return repository.save(region);
			
		}
		return null;
		
	}

	@Override
	@Transactional
	public boolean deleteById(Long id) {

		Optional<Region> regionOP = this.repository.findById(id);

		if (regionOP.isPresent()) {
			
			repository.deleteById(id);
			return true;
		}
		return false;

	}




}


	
	

	


