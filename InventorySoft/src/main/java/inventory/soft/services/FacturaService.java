package inventory.soft.services;

import java.util.Optional;

import org.springframework.data.domain.Pageable;

import inventory.soft.entity.Factura;



public interface FacturaService {
	
	public Iterable<Factura>findAll(Pageable pageable);
	
	public Optional<Factura>findbyId(Long id);
	
	public Factura save(Factura factura);
	
	public boolean deleteById(Long id);

}
