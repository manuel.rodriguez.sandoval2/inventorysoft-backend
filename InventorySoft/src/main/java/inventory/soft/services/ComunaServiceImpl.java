package inventory.soft.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import inventory.soft.entity.Comuna;
import inventory.soft.repository.ComunaRepository;


@Service
public class ComunaServiceImpl implements ComunaService {

	@Autowired
	private ComunaRepository repository;
	@Transactional(readOnly=true)
	@Override
	public Iterable<Comuna> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return repository.findAll(pageable);
	}
	
	

	

	@Override
	@Transactional(readOnly=true)
	public Optional<Comuna> findbyId(Long id) {
		// TODO Auto-generated method stub
		return repository.findById(id);
		
	}

	@Override
	@Transactional
	public Comuna save(Comuna comuna) {
		// TODO Auto-generated method stub
		
		Comuna comunaCod = this.repository.findComunaById(comuna.getId());
		
		if (comunaCod==null) {
			return repository.save(comuna);
			
		}
		return null;
		
	}

	@Override
	@Transactional
	public boolean deleteById(Long id) {

		Optional<Comuna> comunaOP = this.repository.findById(id);

		if (comunaOP.isPresent()) {
			
			repository.deleteById(id);
			return true;
		}
		return false;

	}





	@Override
	@Transactional(readOnly=true)
	public List<Comuna> findByNombre() {
		// TODO Auto-generated method stub
		return repository.findByNombre();
	}




}


	
	

	


