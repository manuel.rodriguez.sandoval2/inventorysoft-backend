package inventory.soft.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import inventory.soft.entity.Cliente;
import inventory.soft.repository.ClienteRepository;

@Service
public class ClienteServiceImpl implements ClienteService {

	@Autowired
	private ClienteRepository repository;

	@Transactional(readOnly = true)
	@Override
	public Iterable<Cliente> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return repository.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<Cliente> findbyId(Long id) {
		// TODO Auto-generated method stub
		return repository.findById(id);

	}

	@Override
	@Transactional
	public Cliente save(Cliente cliente) {
		// TODO Auto-generated method stub

		Cliente clienteCod = this.repository.findClienteByRut(cliente.getRutCliente());

		if (clienteCod == null) {
			return repository.save(cliente);

		}
		return null;

	}

	@Override
	@Transactional
	public boolean deleteById(Long id) {

		Optional<Cliente> clienteOP = this.repository.findById(id);

		if (clienteOP.isPresent()) {

			repository.deleteById(id);
			return true;
		}
		return false;

	}

	@Override
	public List<Cliente> findByNombre() {
		// TODO Auto-generated method stub
		return repository.findByNombre();
	}

}
