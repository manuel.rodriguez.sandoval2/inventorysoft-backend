package inventory.soft.services;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;

import inventory.soft.entity.Comuna;

public interface ComunaService {
	
public Iterable<Comuna>findAll(Pageable pageable);
	
	public Optional<Comuna>findbyId(Long id);
	
	public Comuna save(Comuna ciudad);
	
	public boolean deleteById(Long id);
	
	public List<Comuna> findByNombre();

}
