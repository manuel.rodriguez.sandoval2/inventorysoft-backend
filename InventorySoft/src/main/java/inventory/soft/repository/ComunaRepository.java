package inventory.soft.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import inventory.soft.entity.Comuna;

@EnableJpaRepositories
public interface ComunaRepository extends PagingAndSortingRepository<Comuna, Long> {
	
	@Query( "SELECT c FROM Comuna c where c.id= :id")
	public Comuna findComunaById(@Param("id") Long id);
	
	@Query( "SELECT new Comuna(c.id, c.nombreComuna) FROM Comuna c ORDER BY c.nombreComuna")
	public List<Comuna> findByNombre();

}
