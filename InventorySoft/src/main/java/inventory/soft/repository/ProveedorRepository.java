package inventory.soft.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import inventory.soft.entity.Proveedor;

@EnableJpaRepositories
public interface ProveedorRepository extends PagingAndSortingRepository<Proveedor, Long> {
	
	@Query( "SELECT p FROM Proveedor p where p.rut= :rut")
	public Proveedor findProveedorByRut(@Param("rut") String string);
	
	@Query( "SELECT new Proveedor(p.id, p.nombreProveedor) FROM Proveedor p")
	public List<Proveedor> findByNombre();

}
