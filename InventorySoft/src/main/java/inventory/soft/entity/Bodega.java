package inventory.soft.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "BODEGA")
public class Bodega implements Serializable {

	private static final long serialVersionUID = 1L;

	// @GeneratedValue(strategy=GenerationType.IDENTITY)
	// @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Id
	@Column(name = "ID_BODEGA")
	private Long id;

	@Column(name = "NOMBRE_BODEGA")
	private String nombreBodega;

	@Column(name = "DIRECCION")
	private String direccion;

	public Bodega(Long id, String nombreBodega) {
		this.id = id;
		this.nombreBodega = nombreBodega;
	}

	@JsonIgnoreProperties(value = { "bodega", "hibernateLazyInitializer", "handler" }, allowSetters = true)
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_comuna")
	private Comuna comunas;

	/*@JsonIgnoreProperties(value = { "bodega", "hibernateLazyInitializer", "handler" }, allowSetters = true)
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "bodega", cascade = CascadeType.ALL)
	private List<Ingreso> ingreso;*/

	public Bodega() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombreBodega() {
		return nombreBodega;
	}

	public void setNombreBodega(String nombreBodega) {
		this.nombreBodega = nombreBodega;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Comuna getComunas() {
		return comunas;
	}

	public void setComunas(Comuna comunas) {
		this.comunas = comunas;
	}

	

	@Override
	public boolean equals(Object obj) {

		if (!(obj instanceof Bodega)) {
			return false;
		}
		Bodega cp = (Bodega) obj;

		return this.id != null && this.id.equals((cp.getId()));

	}

}
