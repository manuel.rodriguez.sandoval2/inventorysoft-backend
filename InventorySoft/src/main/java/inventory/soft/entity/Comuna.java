package inventory.soft.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "COMUNA")
public class Comuna implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id_comuna")
	private Long id;

	@Column(name = "nombre_comuna")
	private String nombreComuna;

	@JsonIgnoreProperties(value = { "comuna", "hibernateLazyInitializer", "handler" }, allowSetters = true)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_provincia")
	private Provincia provincia;

	

	public Comuna(Long id, String nombreComuna, Provincia provincia) {
		super();
		this.id = id;
		this.nombreComuna = nombreComuna;
		this.provincia = provincia;
	}

	public Comuna(Long id, String nombreComuna) {
		super();
		this.id = id;
		this.nombreComuna = nombreComuna;
	}

	public Comuna() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombreComuna() {
		return nombreComuna;
	}

	public void setNombreComuna(String nombreComuna) {
		this.nombreComuna = nombreComuna;
	}

	public Provincia getProvincia() {
		return provincia;
	}

	public void setProvincia(Provincia provincia) {
		this.provincia = provincia;
	}



}
