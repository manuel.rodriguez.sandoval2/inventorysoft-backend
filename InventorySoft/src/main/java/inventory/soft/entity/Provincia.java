package inventory.soft.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "PROVINCIA")
public class Provincia implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id_provincia")
	private Long id;

	@Column(name = "nombre_provincia")
	private String nombreProvincia;

	@JsonIgnoreProperties(value = { "provincia", "hibernateLazyInitializer", "handler" }, allowSetters = true)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_region")
	private Region region;
	
	
	@JsonIgnoreProperties(value = { "provincia", "hibernateLazyInitializer", "handler" }, allowSetters = true)
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "provincia", cascade = CascadeType.ALL)
	private List<Comuna> comuna;

	public Provincia() {
		this.comuna=new ArrayList<>();
	}

	public Provincia(Long id, String nombreProvincia, Region region) {
		super();
		this.id = id;
		this.nombreProvincia = nombreProvincia;
		this.region = region;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombreProvincia() {
		return nombreProvincia;
	}

	public void setNombreProvincia(String nombreProvincia) {
		this.nombreProvincia = nombreProvincia;
	}

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

}
