package inventory.soft.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "stock")
public class Stock implements Serializable {

	@Id
	@Column(name = "ID_STOCK")
	private Long id;

	@Column(name = "cantidad_productos")
	private Long cantidadProductos;

	// producto
	@JsonIgnoreProperties(value = { "stock", "hibernateLazyInitializer", "handler" }, allowSetters = true)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_producto")
	private Producto producto;

	// bodega
	@JsonIgnoreProperties(value = { "stock", "hibernateLazyInitializer", "handler" }, allowSetters = true)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_bodega")
	private Bodega bodega;

	public Stock() {
	}

	public Stock(Long id, Long cantidadProductos, Producto producto, Bodega bodega) {
		super();
		this.id = id;
		this.cantidadProductos = cantidadProductos;
		this.producto = producto;
		this.bodega = bodega;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCantidadProductos() {
		return cantidadProductos;
	}

	public void setCantidadProductos(Long cantidadProductos) {
		this.cantidadProductos = cantidadProductos;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public Bodega getBodega() {
		return bodega;
	}

	public void setBodega(Bodega bodega) {
		this.bodega = bodega;
	}

	private static final long serialVersionUID = 1L;

}
