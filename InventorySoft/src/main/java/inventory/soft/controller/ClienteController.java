package inventory.soft.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import inventory.soft.entity.Cliente;
import inventory.soft.services.ClienteService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class ClienteController {

	@Autowired
	private ClienteService clienteService;

	@GetMapping("/cliente/listar")
	public ResponseEntity<?> listar(Pageable pageable) {
		return ResponseEntity.ok().body(clienteService.findAll(pageable));

	}

	@GetMapping("/cliente/listar/{id}")
	public ResponseEntity<?> ver(@PathVariable Long id) {

		Optional<Cliente> b = clienteService.findbyId(id);
		if (b.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(b.get());
	}

	@GetMapping("/cliente/filtrar")
	public ResponseEntity<?> filtrar() {

		return ResponseEntity.ok(clienteService.findByNombre());

	}

	@PostMapping("/cliente/ingresar")
	public ResponseEntity<?> crear(@RequestBody Cliente cliente) {
		Cliente clienteBd = clienteService.save(cliente);
		if (clienteBd == null) {

			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(clienteBd);
	}

	@PutMapping("/cliente/modificar/{id}")
	public ResponseEntity<?> editar(@RequestBody Cliente cliente, @PathVariable Long id) {

		Optional<Cliente> b = clienteService.findbyId(id);
		if (b.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		Cliente clienteBd = b.get();
		clienteBd.setId(cliente.getId());
		clienteBd.setRutCliente(cliente.getRutCliente());
		clienteBd.setNombreCliente(cliente.getNombreCliente());
		clienteBd.setApellidoPaterno(cliente.getApellidoPaterno());
		clienteBd.setApellidoMaterno(cliente.getApellidoMaterno());
		clienteBd.setDireccion(cliente.getDireccion());
		clienteBd.setEmail(cliente.getEmail());
		clienteBd.setDescCliente(cliente.getDescCliente());
		clienteBd.setComunas(cliente.getComunas());

		return ResponseEntity.status(HttpStatus.CREATED).body(clienteService.save(clienteBd));
	}

	@DeleteMapping("/cliente/eliminar/{id}")
	public ResponseEntity<?> eliminar(@PathVariable Long id) {

		boolean isDeleted = clienteService.deleteById(id);
		if (isDeleted) {
			return ResponseEntity.noContent().build();
		}

		return ResponseEntity.notFound().build();

	}

}
