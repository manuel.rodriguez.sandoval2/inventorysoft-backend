package inventory.soft.controller;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import inventory.soft.entity.Bodega;
import inventory.soft.services.BodegaService;


@CrossOrigin(origins="*")
@RestController
@RequestMapping("/api")
public class BodegaController {

	@Autowired
	private BodegaService bodegaService;
	
	
	@GetMapping("/bodega/listar")
	public ResponseEntity<?> listar(Pageable pageable) {
		return ResponseEntity.ok().body(bodegaService.findAll(pageable));
	
	}
	
	@GetMapping("/bodega/listado")
	public ResponseEntity<?> listado() {
		return ResponseEntity.ok().body(bodegaService.findProductoByBodega());
	
	}

	@GetMapping("/bodega/listar/{id}")
	public ResponseEntity<?> ver(@PathVariable Long id) {

		Optional<Bodega> b = bodegaService.findbyId(id);
		if (b.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(b.get());
	}
	
	@GetMapping("/bodega/filtrar")
	public ResponseEntity<?> filtrar() {

		return ResponseEntity.ok(bodegaService.findByNombre());
	
	}

	@PostMapping("/bodega/ingresar")
	public ResponseEntity<?> crear(@RequestBody Bodega bodega) {
		Bodega bodegaDb = bodegaService.save(bodega);
		if(bodegaDb==null) {
			
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(bodegaDb);
	}
	
	
	/*@PutMapping("/{id}/agregar-producto")
	public ResponseEntity<?> agregarProducto(@RequestBody List<Producto>productos, @PathVariable Long id) {

		Optional<Bodega> b = bodegaService.findbyId(id);
		if (!b.isPresent()) {
			return ResponseEntity.notFound().build();
		}

		Bodega bodegaDb = b.get();
		
		productos.forEach(p->{
			bodegaDb.addProductos(p);
		});
		
		return ResponseEntity.status(HttpStatus.CREATED).body(bodegaService.save(bodegaDb));
	}*/
	
	@PutMapping("/bodega/modificar/{id}")
	public ResponseEntity<?> editar(@RequestBody Bodega bodega, @PathVariable Long id) {

		Optional<Bodega> b = bodegaService.findbyId(id);
		if (b.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		Bodega bodegaDb = b.get();
		bodegaDb.setId(bodega.getId());
		bodegaDb.setDireccion(bodega.getDireccion());
		bodegaDb.setNombreBodega(bodega.getNombreBodega());
		
		return ResponseEntity.status(HttpStatus.CREATED).body(bodegaService.save(bodegaDb));
	}

	@DeleteMapping("/bodega/eliminar/{id}")
	public ResponseEntity<?> eliminar(@PathVariable Long id) {
		
		boolean isDeleted=bodegaService.deleteById(id);
		if(isDeleted) {
			return ResponseEntity.noContent().build();
		}
		
		return ResponseEntity.notFound().build();

	}

	
	/*@PutMapping("/{id}/eliminar-producto")
	public ResponseEntity<?> eliminarProducto(@RequestBody Producto producto, @PathVariable Long id) {

		Optional<Bodega> b = bodegaService.findbyId(id);
		if (!b.isPresent()) {
			return ResponseEntity.notFound().build();
		}

		Bodega bodegaDb = b.get();
		
		bodegaDb.removeProductos(producto);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(bodegaService.save(bodegaDb));
	}
	
	@GetMapping("/producto/{id}")
	public ResponseEntity<?> buscarProductoById(@PathVariable Long id) {

		Bodega b = bodegaService.findBodegaByProductoId(id);
		return ResponseEntity.ok(b);
	}*/



}
