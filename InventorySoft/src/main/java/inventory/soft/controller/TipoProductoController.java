package inventory.soft.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import inventory.soft.entity.TipoProducto;
import inventory.soft.services.TipoProductoService;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/tipoproducto")
public class TipoProductoController {
	
	@Autowired
	private TipoProductoService tipoProductoService;
	
	
	@GetMapping("/listar")
	public ResponseEntity<?> listar(Pageable pageable) {
		return ResponseEntity.ok().body(tipoProductoService.findAll(pageable));
	
	}

	@GetMapping("/listar/{id}")
	public ResponseEntity<?> ver(@PathVariable Long id) {

		Optional<TipoProducto> b = tipoProductoService.findbyId(id);
		if (b.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(b.get());
	}
	
	

	@PostMapping("/ingresar")
	public ResponseEntity<?> crear(@RequestBody TipoProducto tipoProducto) {
		TipoProducto tipoProductoDb = tipoProductoService.save(tipoProducto);
		if(tipoProductoDb==null) {
			
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(tipoProductoDb);
	}
	
	@PutMapping("/modificar/{id}")
	public ResponseEntity<?> editar(@RequestBody TipoProducto tipoProducto, @PathVariable Long id) {

		Optional<TipoProducto> b = tipoProductoService.findbyId(id);
		if (b.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		TipoProducto tipoProductoDb = b.get();
		tipoProductoDb.setIdTipoProducto(tipoProducto.getIdTipoProducto());
		tipoProductoDb.setNombreTipoProducto(tipoProducto.getNombreTipoProducto());
		
		return ResponseEntity.status(HttpStatus.CREATED).body(tipoProductoService.save(tipoProductoDb));
	}

	@DeleteMapping("/eliminar/{id}")
	public ResponseEntity<?> eliminar(@PathVariable Long id) {
		
		boolean isDeleted=tipoProductoService.deleteById(id);
		if(isDeleted) {
			return ResponseEntity.noContent().build();
		}
		
		return ResponseEntity.notFound().build();

	}

	
	



}






