package inventory.soft.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import inventory.soft.entity.Comuna;
import inventory.soft.services.ComunaService;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/api")
public class ComunaController {

	@Autowired
	private ComunaService comunaService;
	
	
	@GetMapping("/comuna//listar")
	public ResponseEntity<?> listar(Pageable pageable) {
		return ResponseEntity.ok().body(comunaService.findAll(pageable));
	
	}

	@GetMapping("/comuna/listar/{id}")
	public ResponseEntity<?> ver(@PathVariable Long id) {

		Optional<Comuna> b = comunaService.findbyId(id);
		if (b.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(b.get());
	}
	
	@GetMapping("/comuna/filtrar")
	public ResponseEntity<?> filtrar() {

		return ResponseEntity.ok(comunaService.findByNombre());
	
	}
	
	

	@PostMapping("/comuna/ingresar")
	public ResponseEntity<?> crear(@RequestBody Comuna Comuna) {
		Comuna comunaBd = comunaService.save(Comuna);
		if(comunaBd==null) {
			
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(comunaBd);
	}
	
	@PutMapping("/comuna/modificar/{id}")
	public ResponseEntity<?> editar(@RequestBody Comuna comuna, @PathVariable Long id) {

		Optional<Comuna> b = comunaService.findbyId(id);
		if (b.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		Comuna comunaBd = b.get();
		comunaBd.setNombreComuna(comuna.getNombreComuna());
		comunaBd.setProvincia(comuna.getProvincia());
		
		
		return ResponseEntity.status(HttpStatus.CREATED).body(comunaService.save(comunaBd));
	}

	@DeleteMapping("/comuna/eliminar/{id}")
	public ResponseEntity<?> eliminar(@PathVariable Long id) {
		
		boolean isDeleted=comunaService.deleteById(id);
		if(isDeleted) {
			return ResponseEntity.noContent().build();
		}
		
		return ResponseEntity.notFound().build();

	}


}
