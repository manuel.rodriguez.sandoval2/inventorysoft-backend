package inventory.soft.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import inventory.soft.entity.Merma;
import inventory.soft.services.MermaService;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/api")
public class MermaController {
	
	@Autowired
	private MermaService mermaService;
	
	
	@GetMapping("/merma/listar")
	public ResponseEntity<?> listar(Pageable pageable) {
		return ResponseEntity.ok().body(mermaService.findAll(pageable));
	
	}

	@GetMapping("/merma/listar/{id}")
	public ResponseEntity<?> ver(@PathVariable Long id) {

		Optional<Merma> b = mermaService.findbyId(id);
		if (b.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(b.get());
	}
	
	

	@PostMapping("/merma/ingresar")
	public ResponseEntity<?> crear(@RequestBody Merma merma) {
		Merma mermaBd = mermaService.save(merma);
		if(mermaBd==null) {
			
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(mermaBd);
	}
	
	@PutMapping("/merma/modificar/{id}")
	public ResponseEntity<?> editar(@RequestBody Merma merma, @PathVariable Long id) {

		Optional<Merma> b = mermaService.findbyId(id);
		if (b.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		Merma mermaBd = b.get();
		mermaBd.setId(merma.getId());
		mermaBd.setCantidad(merma.getCantidad());		
		mermaBd.setFechaBaja(merma.getFechaBaja());
		
		
		return ResponseEntity.status(HttpStatus.CREATED).body(mermaService.save(mermaBd));
	}

	@DeleteMapping("/merma/eliminar/{id}")
	public ResponseEntity<?> eliminar(@PathVariable Long id) {
		
		boolean isDeleted=mermaService.deleteById(id);
		if(isDeleted) {
			return ResponseEntity.noContent().build();
		}
		
		return ResponseEntity.notFound().build();

	}


}
