package inventory.soft.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import inventory.soft.entity.Usuario;
import inventory.soft.services.UsuarioService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")

public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService;

	@GetMapping("/usuario/listar")
	public ResponseEntity<?> listar(Pageable pageable) {
		return ResponseEntity.ok().body(usuarioService.findAll(pageable));

	}

	@GetMapping("/usuario/listar/{id}")
	public ResponseEntity<?> ver(@PathVariable Long id) {

		Optional<Usuario> b = usuarioService.findbyId(id);
		if (b.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(b.get());
	}

	@GetMapping("/usuario/filtrar")
	public ResponseEntity<?> filtrar() {

		return ResponseEntity.ok(usuarioService.findByNombre());

	}

	@PostMapping("/usuario/ingresar")
	public ResponseEntity<?> crear(@RequestBody Usuario usuario) {
		Usuario usuarioBd = usuarioService.save(usuario);
		if (usuarioBd == null) {

			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(usuarioBd);
	}

	@PutMapping("/usuario/modificar/{id}")
	public ResponseEntity<?> editar(@RequestBody Usuario usuario, @PathVariable Long id) {

		Optional<Usuario> b = usuarioService.findbyId(id);
		if (b.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		Usuario usuarioBd = b.get();
		usuarioBd.setId(usuario.getId());
		usuarioBd.setRut(usuario.getRut());
		usuarioBd.setNombreUsuario(usuario.getNombreUsuario());
		usuarioBd.setApellidoPaterno(usuario.getApellidoPaterno());
		usuarioBd.setApellidoMaterno(usuario.getApellidoMaterno());
		usuarioBd.setContrasena(usuario.getContrasena());
		usuarioBd.setUsername(usuario.getUsername());
		usuarioBd.setDireccion(usuario.getDireccion());
		usuarioBd.setEmail(usuario.getEmail());
		// usuarioBd.setTipoUsuario(usuario.getTipoUsuario());
		usuarioBd.setComunas(usuario.getComunas());

		return ResponseEntity.status(HttpStatus.CREATED).body(usuarioService.save(usuarioBd));
	}

	@DeleteMapping("/usuario/eliminar/{id}")
	public ResponseEntity<?> eliminar(@PathVariable Long id) {

		boolean isDeleted = usuarioService.deleteById(id);
		if (isDeleted) {
			return ResponseEntity.noContent().build();
		}

		return ResponseEntity.notFound().build();

	}

}
