package inventory.soft.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import inventory.soft.entity.Provincia;
import inventory.soft.services.ProvinciaService;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/provincia")
public class ProvinciaController {
	
	
	@Autowired
	private ProvinciaService provinciaService;
	
	
	@GetMapping("/listar")
	public ResponseEntity<?> listar(Pageable pageable) {
		return ResponseEntity.ok().body(provinciaService.findAll(pageable));
	
	}

	@GetMapping("/listar/{id}")
	public ResponseEntity<?> ver(@PathVariable Long id) {

		Optional<Provincia> b = provinciaService.findbyId(id);
		if (b.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(b.get());
	}
	
	

	@PostMapping("/ingresar")
	public ResponseEntity<?> crear(@RequestBody Provincia Provincia) {
		Provincia ProvinciaBd = provinciaService.save(Provincia);
		if(ProvinciaBd==null) {
			
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(ProvinciaBd);
	}
	
	@PutMapping("/modificar/{id}")
	public ResponseEntity<?> editar(@RequestBody Provincia provincia, @PathVariable Long id) {

		Optional<Provincia> b = provinciaService.findbyId(id);
		if (b.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		Provincia provinciaBd = b.get();
		provinciaBd.setNombreProvincia(provincia.getNombreProvincia());
		provinciaBd.setRegion(provincia.getRegion());
		
		return ResponseEntity.status(HttpStatus.CREATED).body(provinciaService.save(provinciaBd));
	}

	@DeleteMapping("/eliminar/{id}")
	public ResponseEntity<?> eliminar(@PathVariable Long id) {
		
		boolean isDeleted=provinciaService.deleteById(id);
		if(isDeleted) {
			return ResponseEntity.noContent().build();
		}
		
		return ResponseEntity.notFound().build();

	}


}





