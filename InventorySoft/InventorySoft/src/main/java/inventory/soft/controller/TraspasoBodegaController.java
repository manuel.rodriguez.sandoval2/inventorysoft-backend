package inventory.soft.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import inventory.soft.entity.Ingreso;
import inventory.soft.entity.TraspasoBodega;
import inventory.soft.services.TraspasoBodegaService;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/api")
public class TraspasoBodegaController {
	
	@Autowired
	private TraspasoBodegaService traspasoBodegaService;
	
	
	@GetMapping("/traspasobodega/listar")
	public ResponseEntity<?> listar(Pageable pageable) {
		return ResponseEntity.ok().body(traspasoBodegaService.findAll(pageable));
	
	}

	@GetMapping("/traspasobodega/listar/{id}")
	public ResponseEntity<?> ver(@PathVariable Long id) {

		Optional<TraspasoBodega> b = traspasoBodegaService.findbyId(id);
		if (b.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(b.get());
	}
	
	

	@PostMapping("/traspasobodega/ingresar")
	public ResponseEntity<?> crear(@RequestBody TraspasoBodega traspasoBodega, Ingreso ingreso) {
		TraspasoBodega traspasoBodegaDb = traspasoBodegaService.save(traspasoBodega);
	//	TraspasoBodega traspasoBodegaDb2 = traspasoBodegaService.update(ingreso);
		if(traspasoBodegaDb==null) {
			
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(traspasoBodegaDb); 
	}
	
	@PutMapping("/traspasobodega/modificar/{id}")
	public ResponseEntity<?> editar(@RequestBody TraspasoBodega traspasoBodega, @PathVariable Long id) {

		Optional<TraspasoBodega> b = traspasoBodegaService.findbyId(id);
		if (b.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		TraspasoBodega traspasoBodegaDb = b.get();
		traspasoBodegaDb.setId(traspasoBodega.getId());
		traspasoBodegaDb.setCantidadProdTraspaso(traspasoBodega.getCantidadProdTraspaso());
		
		
		
		traspasoBodegaDb.setFechaTraspaso(traspasoBodega.getFechaTraspaso());
		
		
		return ResponseEntity.status(HttpStatus.CREATED).body(traspasoBodegaService.save(traspasoBodegaDb));
	}

	@DeleteMapping("/traspasobodega/eliminar/{id}")
	public ResponseEntity<?> eliminar(@PathVariable Long id) {
		
		boolean isDeleted=traspasoBodegaService.deleteById(id);
		if(isDeleted) {
			return ResponseEntity.noContent().build();
		}
		
		return ResponseEntity.notFound().build();

	}

	
	



}


