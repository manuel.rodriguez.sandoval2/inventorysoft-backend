package inventory.soft.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import inventory.soft.entity.Proveedor;
import inventory.soft.services.ProveedorService;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/api")
public class ProveedorController {
	
	@Autowired
	private ProveedorService proveedorService;
	
	
	@GetMapping("/proveedor/listar")
	public ResponseEntity<?> listar(Pageable pageable) {
		return ResponseEntity.ok().body(proveedorService.findAll(pageable));
	
	}

	@GetMapping("/proveedor/listar/{id}")
	public ResponseEntity<?> ver(@PathVariable Long id) {

		Optional<Proveedor> b = proveedorService.findbyId(id);
		if (b.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(b.get());
	}
	
	@GetMapping("/proveedor/filtrar")
	public ResponseEntity<?> filtrar() {

		return ResponseEntity.ok(proveedorService.findByNombre());
	
	}
	
	

	@PostMapping("/proveedor/ingresar")
	public ResponseEntity<?> crear(@RequestBody Proveedor proveedor) {
		Proveedor proveedorBd = proveedorService.save(proveedor);
		if(proveedorBd==null) {
			
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(proveedorBd);
	}
	
	@PutMapping("/proveedor/modificar/{id}")
	public ResponseEntity<?> editar(@RequestBody Proveedor proveedor, @PathVariable Long id) {

		Optional<Proveedor> b = proveedorService.findbyId(id);
		if (b.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		Proveedor proveedorBd = b.get();
		proveedorBd.setId(proveedor.getId());
		proveedorBd.setRut(proveedor.getRut());
		proveedorBd.setNombreProveedor(proveedor.getNombreProveedor());
		proveedorBd.setDireccion(proveedor.getDireccion());
		proveedorBd.setEmail(proveedor.getEmail());
		proveedorBd.setGiro(proveedor.getGiro());
		proveedorBd.setComunas(proveedor.getComunas());
		
		return ResponseEntity.status(HttpStatus.CREATED).body(proveedorService.save(proveedorBd));
	}

	@DeleteMapping("/proveedor/eliminar/{id}")
	public ResponseEntity<?> eliminar(@PathVariable Long id) {
		
		boolean isDeleted=proveedorService.deleteById(id);
		if(isDeleted) {
			return ResponseEntity.noContent().build();
		}
		
		return ResponseEntity.notFound().build();

	}


}





