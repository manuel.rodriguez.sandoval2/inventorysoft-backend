package inventory.soft.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import inventory.soft.entity.Producto;
import inventory.soft.services.ProductoService;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/api")
public class ProductoController {
	
	
	@Autowired
	private ProductoService productoService;
	
	
	@GetMapping("/producto/listar")
	public ResponseEntity<?> listar(Pageable pageable) {
		return ResponseEntity.ok().body(productoService.findAll(pageable));
	
	}

	@GetMapping("/producto/listar/{id}")
	public ResponseEntity<?> ver(@PathVariable Long id) {

		Optional<Producto> b = productoService.findbyId(id);
		if (b.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(b.get());
	}
	
	@GetMapping("/producto/filtrar")
	public ResponseEntity<?> filtrar() {

		return ResponseEntity.ok(productoService.findByNombre());
	
	}
	
	

	@PostMapping("/producto/ingresar")
	public ResponseEntity<?> crear(@RequestBody Producto producto) {
		Producto productoBd = productoService.save(producto);
		if(productoBd==null) {
			
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(productoBd);
	}
	
	@PutMapping("/producto/modificar/{id}")
	public ResponseEntity<?> editar(@RequestBody Producto producto, @PathVariable Long id) {

		Optional<Producto> b = productoService.findbyId(id);
		if (b.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		Producto productoBd = b.get();
		productoBd.setIdProducto(producto.getIdProducto());
		productoBd.setMarcaProducto(producto.getMarcaProducto());
		productoBd.setNombreProducto(producto.getNombreProducto());
		return ResponseEntity.status(HttpStatus.CREATED).body(productoService.save(productoBd));
	}

	@DeleteMapping("/producto/eliminar/{id}")
	public ResponseEntity<?> eliminar(@PathVariable Long id) {
		
		boolean isDeleted=productoService.deleteById(id);
		if(isDeleted) {
			return ResponseEntity.noContent().build();
		}
		
		return ResponseEntity.notFound().build();

	}


}



