package inventory.soft.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import inventory.soft.entity.Factura;
import inventory.soft.services.FacturaService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class FacturaController {

	@Autowired
	private FacturaService facturaService;

	@GetMapping("/factura/listar")
	public ResponseEntity<?> listar(Pageable pageable) {
		return ResponseEntity.ok().body(facturaService.findAll(pageable));

	}

	@GetMapping("/factura/listar/{id}")
	public ResponseEntity<?> ver(@PathVariable Long id) {

		Optional<Factura> b = facturaService.findbyId(id);
		if (b.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(b.get());
	}

	@PostMapping("/factura/ingresar")
	public ResponseEntity<?> crear(@RequestBody Factura factura) {
		Factura facturaBd = facturaService.save(factura);
		if (facturaBd == null) {

			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(facturaBd);
	}

	@PutMapping("/factura/modificar/{id}")
	public ResponseEntity<?> editar(@RequestBody Factura factura, @PathVariable Long id) {

		Optional<Factura> b = facturaService.findbyId(id);
		if (b.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		Factura facturaBd = b.get();
		facturaBd.setId(factura.getId());
		facturaBd.setCantidad(factura.getCantidad());
		facturaBd.setNumeroDocumento(factura.getNumeroDocumento());
		facturaBd.setTipoDocumento(factura.getTipoDocumento());

		return ResponseEntity.status(HttpStatus.CREATED).body(facturaService.save(facturaBd));
	}

	@DeleteMapping("/factura/eliminar/{id}")
	public ResponseEntity<?> eliminar(@PathVariable Long id) {

		boolean isDeleted = facturaService.deleteById(id);
		if (isDeleted) {
			return ResponseEntity.noContent().build();
		}

		return ResponseEntity.notFound().build();

	}

}
