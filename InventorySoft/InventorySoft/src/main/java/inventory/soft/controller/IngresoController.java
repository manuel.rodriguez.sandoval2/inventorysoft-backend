package inventory.soft.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import inventory.soft.entity.Ingreso;
import inventory.soft.entity.Producto;
import inventory.soft.services.IngresoService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class IngresoController {

	@Autowired
	private IngresoService ingresoService;

	@GetMapping("/ingreso/listar")
	public ResponseEntity<?> listar(Pageable pageable) {
		return ResponseEntity.ok().body(ingresoService.findAll(pageable));

	}

	@GetMapping("/ingreso/listar/{id}")
	public ResponseEntity<?> ver(@PathVariable Long id) {

		Optional<Ingreso> b = ingresoService.findbyId(id);
		if (b.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(b.get());
	}
	
	@GetMapping("/ingreso/producto/{id}")
	public List<Producto>filtrarProductos(@PathVariable Long id){
		return ingresoService.findByCodigo(id);
		
	}

	@PostMapping("/ingreso/ingresar")
	public ResponseEntity<?> crear(@RequestBody Ingreso ingreso) {
		Ingreso ingresoBd = ingresoService.save(ingreso);
		if (ingresoBd == null) {

			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(ingresoBd);
	}

	@PutMapping("/ingreso/modificar/{id}")
	public ResponseEntity<?> editar(@RequestBody Ingreso ingreso, @PathVariable Long id) {

		Optional<Ingreso> b = ingresoService.findbyId(id);
		if (b.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		Ingreso ingresoBd = b.get();
		ingresoBd.setId(ingreso.getId());
	//	ingresoBd.setCantidadProducto(ingreso.getCantidadProducto());
		ingresoBd.setBodega(ingreso.getBodega());
		ingresoBd.setFechaIngreso(ingreso.getFechaIngreso());

		return ResponseEntity.status(HttpStatus.CREATED).body(ingresoService.save(ingresoBd));
	}

	@DeleteMapping("/ingreso/eliminar/{id}")
	public ResponseEntity<?> eliminar(@PathVariable Long id) {

		boolean isDeleted = ingresoService.deleteById(id);
		if (isDeleted) {
			return ResponseEntity.noContent().build();
		}

		return ResponseEntity.notFound().build();

	}

}
