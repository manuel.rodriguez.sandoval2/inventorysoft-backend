package inventory.soft.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import inventory.soft.entity.Region;
import inventory.soft.services.RegionService;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/region")
public class RegionController {

	@Autowired
	private RegionService regionService;
	
	
	@GetMapping("/listar")
	public ResponseEntity<?> listar(Pageable pageable) {
		return ResponseEntity.ok().body(regionService.findAll(pageable));
	
	}

	@GetMapping("/listar/{id}")
	public ResponseEntity<?> ver(@PathVariable Long id) {

		Optional<Region> b = regionService.findbyId(id);
		if (b.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(b.get());
	}
	
	

	@PostMapping("/ingresar")
	public ResponseEntity<?> crear(@RequestBody Region region) {
		Region regionBd = regionService.save(region);
		if(regionBd==null) {
			
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(regionBd);
	}
	
	@PutMapping("/modificar/{id}")
	public ResponseEntity<?> editar(@RequestBody Region region, @PathVariable Long id) {

		Optional<Region> b = regionService.findbyId(id);
		if (b.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		Region regionBd = b.get();
		regionBd.setNombreRegion(region.getNombreRegion());
		regionBd.setNumeroRegion(region.getNumeroRegion());
		
		return ResponseEntity.status(HttpStatus.CREATED).body(regionService.save(regionBd));
	}

	@DeleteMapping("/eliminar/{id}")
	public ResponseEntity<?> eliminar(@PathVariable Long id) {
		
		boolean isDeleted=regionService.deleteById(id);
		if(isDeleted) {
			return ResponseEntity.noContent().build();
		}
		
		return ResponseEntity.notFound().build();

	}


}

