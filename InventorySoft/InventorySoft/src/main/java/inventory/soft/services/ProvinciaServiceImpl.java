package inventory.soft.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import inventory.soft.entity.Provincia;
import inventory.soft.repository.ProvinciaRepository;


@Service
public class ProvinciaServiceImpl implements ProvinciaService {

	@Autowired
	private ProvinciaRepository repository;
	@Transactional(readOnly=true)
	@Override
	public Iterable<Provincia> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return repository.findAll(pageable);
	}
	
	

	

	@Override
	@Transactional(readOnly=true)
	public Optional<Provincia> findbyId(Long id) {
		// TODO Auto-generated method stub
		return repository.findById(id);
		
	}

	@Override
	@Transactional
	public Provincia save(Provincia provincia) {
		// TODO Auto-generated method stub
		
		Provincia provinciaCod = this.repository.findProvinciaById(provincia.getId());
		
		if (provinciaCod==null) {
			return repository.save(provincia);
			
		}
		return null;
		
	}

	@Override
	@Transactional
	public boolean deleteById(Long id) {

		Optional<Provincia> provinciaOP = this.repository.findById(id);

		if (provinciaOP.isPresent()) {
			
			repository.deleteById(id);
			return true;
		}
		return false;

	}




}


	
	

	


