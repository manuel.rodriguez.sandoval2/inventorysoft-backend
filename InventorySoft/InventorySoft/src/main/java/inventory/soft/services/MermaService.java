package inventory.soft.services;

import java.util.Optional;

import org.springframework.data.domain.Pageable;

import inventory.soft.entity.Merma;





public interface MermaService {
	

	
	public Iterable<Merma>findAll(Pageable pageable);
	
	public Optional<Merma>findbyId(Long id);
	
	public Merma save(Merma merma);
	
	public boolean deleteById(Long id);

}
