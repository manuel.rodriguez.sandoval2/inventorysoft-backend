package inventory.soft.services;

import java.util.Optional;

import org.springframework.data.domain.Pageable;

import inventory.soft.entity.TipoProducto;

public interface TipoProductoService {
	
	
	public Iterable<TipoProducto>findAll(Pageable pageable);
	
	public Optional<TipoProducto>findbyId(Long id);
	
	public TipoProducto save(TipoProducto tipoProducto);
	
	public boolean deleteById(Long id);

}
