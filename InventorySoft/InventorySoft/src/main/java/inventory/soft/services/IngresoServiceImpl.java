package inventory.soft.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import inventory.soft.entity.Ingreso;
import inventory.soft.entity.Producto;
import inventory.soft.repository.IngresoRepository;
import inventory.soft.repository.ProductoRepository;

@Service
public class IngresoServiceImpl implements IngresoService {

	@Autowired
	private IngresoRepository repository;

	@Autowired
	private ProductoRepository productoRepository;

	@Transactional(readOnly = true)
	@Override
	public Iterable<Ingreso> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<Ingreso> findbyId(Long id) {
		return repository.findById(id);

	}

	@Override
	@Transactional
	public Ingreso save(Ingreso ingreso) {

		Ingreso ingresoCod = this.repository.findIngresoById(ingreso.getId());

		if (ingresoCod == null) {
			return repository.save(ingreso);

		}
		return null;

	}

	@Override
	@Transactional
	public boolean deleteById(Long id) {

		Optional<Ingreso> ingresoOP = this.repository.findById(id);

		if (ingresoOP.isPresent()) {
			repository.deleteById(id);
			return true;
		}
		return false;

	}

	@Override
	@Transactional(readOnly = true)
	public List<Producto> findByCodigo(Long id) {
		return productoRepository.findByCodigo(id);
	}

}
