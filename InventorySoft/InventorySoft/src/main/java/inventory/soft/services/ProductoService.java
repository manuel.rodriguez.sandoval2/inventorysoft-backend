package inventory.soft.services;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;

import inventory.soft.entity.Producto;



public interface ProductoService {
	
	public Iterable<Producto>findAll(Pageable pageable);
	
	public Optional<Producto>findbyId(Long id);
	
	public Producto save(Producto producto);
	
	public boolean deleteById(Long id);
	
	public List<Producto> findByNombre();

}
