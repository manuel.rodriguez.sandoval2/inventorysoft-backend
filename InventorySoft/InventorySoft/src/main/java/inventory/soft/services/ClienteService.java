package inventory.soft.services;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;

import inventory.soft.entity.Cliente;

public interface ClienteService {
	
public Iterable<Cliente>findAll(Pageable pageable);
	
	public Optional<Cliente>findbyId(Long id);
	
	public Cliente save(Cliente cliente);

	public boolean deleteById(Long id);
	
	public List<Cliente> findByNombre();

}
