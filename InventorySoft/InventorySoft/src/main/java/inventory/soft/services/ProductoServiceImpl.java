package inventory.soft.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import inventory.soft.entity.Producto;
import inventory.soft.repository.ProductoRepository;


@Service
public class ProductoServiceImpl implements ProductoService {

	@Autowired
	private ProductoRepository repository;
	@Transactional(readOnly=true)
	@Override
	public Iterable<Producto> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return repository.findAll(pageable);
	}
	


	@Override
	@Transactional(readOnly=true)
	public Optional<Producto> findbyId(Long id) {
		// TODO Auto-generated method stub
		return repository.findById(id);
		
	}

	@Override
	@Transactional
	public Producto save(Producto producto) {
		// TODO Auto-generated method stub
		
		Producto productoCod = this.repository.findProductoById(producto.getIdProducto());
		
		if (productoCod==null) {
			return repository.save(producto);
			
		}
		return null;
		
	}

	@Override
	@Transactional
	public boolean deleteById(Long id) {

		Optional<Producto> productoOP = this.repository.findById(id);

		if (productoOP.isPresent()) {
			repository.deleteById(id);
			return true;
		}
		return false;

	}



	@Override
	@Transactional
	public List<Producto> findByNombre() {
		// TODO Auto-generated method stub
		return repository.findByNombre();
	}

}


	
	

	







