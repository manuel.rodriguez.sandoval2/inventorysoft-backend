package inventory.soft.services;

import java.util.Optional;

import org.springframework.data.domain.Pageable;

import inventory.soft.entity.Provincia;

public interface ProvinciaService {
	
	
	
	
public Iterable<Provincia>findAll(Pageable pageable);
	
	public Optional<Provincia>findbyId(Long id);
	
	public Provincia save(Provincia provincia);
	
	public boolean deleteById(Long id);

}
