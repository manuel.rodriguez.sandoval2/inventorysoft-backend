package inventory.soft.services;

import java.util.Optional;

import org.springframework.data.domain.Pageable;


import inventory.soft.entity.Region;

public interface RegionService {
	
	
public Iterable<Region>findAll(Pageable pageable);
	
	public Optional<Region>findbyId(Long id);
	
	public Region save(Region region);
	
	public boolean deleteById(Long id);

}
