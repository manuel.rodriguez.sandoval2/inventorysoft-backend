package inventory.soft.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import inventory.soft.entity.Merma;
import inventory.soft.repository.MermaRepository;


@Service
public class MermaServiceImpl implements MermaService {

	@Autowired
	private MermaRepository repository;
	@Transactional(readOnly=true)
	@Override
	public Iterable<Merma> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return repository.findAll(pageable);
	}
	


	@Override
	@Transactional(readOnly=true)
	public Optional<Merma> findbyId(Long id) {
		// TODO Auto-generated method stub
		return repository.findById(id);
		
	}

	@Override
	@Transactional
	public Merma save(Merma merma) {
		// TODO Auto-generated method stub
		
		Merma mermaCod = this.repository.findMermaById(merma.getId());
		
		if (mermaCod==null) {
			return repository.save(merma);
			
		}
		return null;
		
	}

	@Override
	@Transactional
	public boolean deleteById(Long id) {

		Optional<Merma> mermaOP = this.repository.findById(id);

		if (mermaOP.isPresent()) {
			repository.deleteById(id);
			return true;
		}
		return false;

	}

}


	
	

	








