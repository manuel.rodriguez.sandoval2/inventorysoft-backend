package inventory.soft.services;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;

import inventory.soft.entity.Ingreso;
import inventory.soft.entity.Producto;

public interface IngresoService {

	public Iterable<Ingreso> findAll(Pageable pageable);

	public Optional<Ingreso> findbyId(Long id);

	public Ingreso save(Ingreso Ingreso);

	public boolean deleteById(Long id);
	
	public List<Producto>findByCodigo(Long id);

}
