package inventory.soft.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import inventory.soft.entity.Proveedor;
import inventory.soft.repository.ProveedorRepository;

@Service
public class ProveedorServiceImpl implements ProveedorService {

	@Autowired
	private ProveedorRepository repository;
	@Transactional(readOnly=true)
	@Override
	public Iterable<Proveedor> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return repository.findAll(pageable);
	}
	
	@Override
	@Transactional(readOnly=true)
	public List<Proveedor> findByNombre() {
		// TODO Auto-generated method stub
		return repository.findByNombre();
	}
	
	@Override
	@Transactional(readOnly=true)
	public Optional<Proveedor> findbyId(Long id) {
		// TODO Auto-generated method stub
		return repository.findById(id);		
	}

	@Override
	@Transactional
	public Proveedor save(Proveedor proveedor) {
		// TODO Auto-generated method stub		
		Proveedor proveedorCod = this.repository.findProveedorByRut(proveedor.getRut());		
		if (proveedorCod==null) {
			return repository.save(proveedor);			
		}
		return null;		
	}

	@Override
	@Transactional
	public boolean deleteById(Long id) {
		Optional<Proveedor> proveedorOP = this.repository.findById(id);
		if (proveedorOP.isPresent()) {			
			repository.deleteById(id);
			return true;
		}
		return false;
	}
}

