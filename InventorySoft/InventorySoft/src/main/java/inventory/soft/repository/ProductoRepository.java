package inventory.soft.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import inventory.soft.entity.Producto;

@EnableJpaRepositories
public interface ProductoRepository extends PagingAndSortingRepository<Producto, Long> {
	
	@Query( "SELECT p FROM Producto p where p.id= :id")
	public Producto findProductoById(@Param("id") Long id);
	
	
	@Query( "SELECT new Producto(p.idProducto, p.nombreProducto) FROM Producto p")
	public List<Producto> findByNombre();
	
	
	@Query( "SELECT p FROM Producto p where p.id= :id")
	public List<Producto>findByCodigo(Long id);
	

}
