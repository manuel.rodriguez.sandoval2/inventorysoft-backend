package inventory.soft.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;


import inventory.soft.entity.Region;



@EnableJpaRepositories
public interface RegionRepository extends PagingAndSortingRepository<Region, Long> {
	
	@Query( "SELECT r FROM Region r where r.id= :id")
	public Region findRegionById(@Param("id") Long id);

}
