package inventory.soft.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;


import inventory.soft.entity.TipoProducto;



@EnableJpaRepositories
public interface TipoProductoRepository extends PagingAndSortingRepository<TipoProducto, Long> {
	
	@Query( "SELECT tp FROM TipoProducto tp where tp.id= :id")
	public TipoProducto findTipoProductoById(@Param("id") Long id);

}
