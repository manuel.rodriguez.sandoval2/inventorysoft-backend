package inventory.soft.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import inventory.soft.entity.Usuario;

@EnableJpaRepositories
public interface UsuarioRepository extends PagingAndSortingRepository<Usuario, Long> {
	
	@Query( "SELECT u FROM Usuario u where u.rut= :rut")
	public Usuario findUsuarioByRut(@Param("rut") String string);
	
	@Query( "SELECT new Usuario(u.id, u.nombreUsuario) FROM Usuario u ORDER BY u.nombreUsuario")
	public List<Usuario> findByNombre();

}
