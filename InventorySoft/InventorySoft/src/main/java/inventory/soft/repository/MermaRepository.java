package inventory.soft.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;


import inventory.soft.entity.Merma;

@EnableJpaRepositories
public interface MermaRepository extends PagingAndSortingRepository<Merma, Long> {
	
	
	@Query( "SELECT m FROM Merma m where m.id= :id")
	public Merma findMermaById(@Param("id") Long id);

}
