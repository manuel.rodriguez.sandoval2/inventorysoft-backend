package inventory.soft.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import inventory.soft.entity.Ingreso;
import inventory.soft.entity.TraspasoBodega;

public interface TraspasoBodegaRepository extends PagingAndSortingRepository<TraspasoBodega, Long> {
	
	@Query( "SELECT tb FROM TraspasoBodega tb where tb.id= :id")
	public TraspasoBodega findTraspasoBodegaById(@Param("id") Long id);
	
	
	//@Modifying
	@Query(value="UPDATE ingreso, traspaso_bodega SET ingreso.cantidad_producto=ingreso.cantidad_producto-traspaso_bodega.cantidad_prod_traspaso "
			+ "WHERE traspaso_bodega.id_producto=ingreso.id_producto AND ingreso.id_bodega=traspaso_bodega.id_bodega_entrada", nativeQuery = true)
	public TraspasoBodega updateCantidad(@Param("cantidadProducto")Ingreso ingreso);


	
	
	
}
