package inventory.soft.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;


import inventory.soft.entity.Factura;



@EnableJpaRepositories
public interface FacturaRepository extends PagingAndSortingRepository<Factura, Long> {
	
	
	@Query( "SELECT s FROM Factura s where s.id= :id")
	public Factura findFacturaById(@Param("id") Long id);

}
