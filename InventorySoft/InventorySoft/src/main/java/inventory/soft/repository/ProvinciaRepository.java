package inventory.soft.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;


import inventory.soft.entity.Provincia;


@EnableJpaRepositories
public interface ProvinciaRepository extends PagingAndSortingRepository<Provincia, Long> {
	
	@Query( "SELECT p FROM Provincia p where p.id= :id")
	public Provincia findProvinciaById(@Param("id") Long id);

}
