package inventory.soft.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import inventory.soft.entity.Ingreso;

@EnableJpaRepositories
public interface IngresoRepository extends PagingAndSortingRepository<Ingreso, Long> {
	
	@Query( "SELECT i FROM Ingreso i where i.id= :id")
	public Ingreso findIngresoById(@Param("id") Long id);

}
