package inventory.soft.repository;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import inventory.soft.entity.Bodega;


@EnableJpaRepositories
public interface BodegaRepository extends PagingAndSortingRepository<Bodega, Long> {
	
	@Query( "SELECT new Bodega(b.id, b.nombreBodega) FROM Bodega b ORDER BY b.nombreBodega")
	public List<Bodega> findByNombre();
	
	//@Query( "SELECT new Bodega(b.id, b.nombreBodega) FROM Bodega b ORDER BY b.nombreBodega")
	public Bodega findBodegaById(@Param("id") Long id);
	
	
	/*
	@Query(value="select b.id_bodega, b.nombre_bodega, b.direccion, c.nombre_comuna "
			+ "from bodega b, comuna c where b.id_comuna = c.id_comuna;", nativeQuery = true)	
	public List<Bodega> findProductoByBodega();*/
	
	//@Query( "SELECT bo FROM Bodega bo join fetch bo.productos a WHERE bo.id=?1")
	//public Bodega findBodegaByProductoId(Long id);

}
