package inventory.soft.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import inventory.soft.entity.Cliente;

@EnableJpaRepositories
public interface ClienteRepository extends PagingAndSortingRepository<Cliente, Long> {
	
	@Query( "SELECT c FROM Cliente c where c.rutCliente= :rutCliente")
	public Cliente findClienteByRut(@Param("rutCliente") String string);
	
	@Query( "SELECT new Cliente(c.id, c.rutCliente, c.nombreCliente, c.apellidoPaterno, c.apellidoMaterno) FROM Cliente c ORDER BY c.nombreCliente")
	public List<Cliente> findByNombre();

}
