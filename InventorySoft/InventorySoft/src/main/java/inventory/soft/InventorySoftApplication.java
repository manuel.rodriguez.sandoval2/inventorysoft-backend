package inventory.soft;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class InventorySoftApplication {

	public static void main(String[] args) {
		SpringApplication.run(InventorySoftApplication.class, args);
	}

}
