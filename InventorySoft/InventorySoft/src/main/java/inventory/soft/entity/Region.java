package inventory.soft.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "REGION")
public class Region implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id_region")
	private Long id;

	@Column(name = "nombre_region")
	private String nombreRegion;

	@Column(name = "numero_region")
	private String numeroRegion;

	@JsonIgnoreProperties(value = { "region", "hibernateLazyInitializer", "handler" }, allowSetters = true)
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "region", cascade = CascadeType.ALL)
	private List<Provincia> provincia;

	public Region(Long id, String nombreRegion, String numeroRegion, List<Provincia> provincia) {
		super();
		this.id = id;
		this.nombreRegion = nombreRegion;
		this.numeroRegion = numeroRegion;
		this.provincia = provincia;
	}

	public Region() {
		this.provincia = new ArrayList<>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombreRegion() {
		return nombreRegion;
	}

	public void setNombreRegion(String nombreRegion) {
		this.nombreRegion = nombreRegion;
	}

	public String getNumeroRegion() {
		return numeroRegion;
	}

	public void setNumeroRegion(String numeroRegion) {
		this.numeroRegion = numeroRegion;
	}

	public List<Provincia> getProvincia() {
		return provincia;
	}

	public void setProvincia(List<Provincia> provincia) {
		this.provincia = provincia;
	}

}
