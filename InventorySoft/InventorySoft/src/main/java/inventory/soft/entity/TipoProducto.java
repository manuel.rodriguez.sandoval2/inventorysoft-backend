package inventory.soft.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "tipo_producto")
public class TipoProducto implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name = "id_tipo_producto")
	private Long idTipoProducto;

	@Column(name = "nombre_tipo_producto")
	private String nombreTipoProducto;

	/*@JsonIgnoreProperties(value = { "tipoProducto", "hibernateLazyInitializer", "handler" }, allowSetters = true)
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "tipoProducto", cascade = CascadeType.ALL)
	private List<Producto> productos;*/

	public TipoProducto() {
		super();
		//this.productos = new ArrayList<>();
	}

	public TipoProducto(Long idTipoProducto, String nombreTipoProducto/*, List<Producto> productos*/) {
		super();
		this.idTipoProducto = idTipoProducto;
		this.nombreTipoProducto = nombreTipoProducto;
		//this.productos = productos;
	}

	public Long getIdTipoProducto() {
		return idTipoProducto;
	}

	public void setIdTipoProducto(Long idTipoProducto) {
		this.idTipoProducto = idTipoProducto;
	}

	public String getNombreTipoProducto() {
		return nombreTipoProducto;
	}

	public void setNombreTipoProducto(String nombreTipoProducto) {
		this.nombreTipoProducto = nombreTipoProducto;
	}

	/*public List<Producto> getProductos() {
		return productos;
	}

	public void setProductos(List<Producto> productos) {
		this.productos = productos;
	}

	public void addProductos(Producto productos) {
		this.productos.add(productos);
	}

	public void removeProductos(Producto productos) {
		this.productos.remove(productos);
	}*/

	@Override
	public boolean equals(Object obj) {

		if (!(obj instanceof TipoProducto)) {
			return false;
		}
		TipoProducto cp = (TipoProducto) obj;

		return this.idTipoProducto != null && this.idTipoProducto.equals((cp.getIdTipoProducto()));

	}

}
