package inventory.soft.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "traspaso_bodega")
public class TraspasoBodega implements Serializable {

	private static final long serialVersionUID = 1L;

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "id_traspaso")
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fecha_traspaso")
	private Date fechaTraspaso;

	@Column(name = "cantidad_prod_traspaso")
	private Long cantidadProdTraspaso;

	@PrePersist
	public void prePersist() {
		this.fechaTraspaso = new Date();
	}

	@JsonIgnoreProperties(value = { "traspasoBodega", "hibernateLazyInitializer", "handler" }, allowSetters = true)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_producto")
	private Producto producto;

	// bodegaOrigen
	@JsonIgnoreProperties(value = { "traspasoBodega", "hibernateLazyInitializer", "handler" }, allowSetters = true)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_bodega_origen")
	private Bodega bodegaOrigen;

	// bodegaDestino
	@JsonIgnoreProperties(value = { "traspasoBodega", "hibernateLazyInitializer", "handler" }, allowSetters = true)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_bodega_destino")
	private Bodega bodegaDestino;

	// usuario
	@JsonIgnoreProperties(value = { "traspasoBodega", "hibernateLazyInitializer", "handler" }, allowSetters = true)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_usuario")
	private Usuario usuario;

	public TraspasoBodega(Long id, Date fechaTraspaso, Long cantidadProdTraspaso, Producto producto,
			Bodega bodegaOrigen, Bodega bodegaDestino, Usuario usuario) {
		super();
		this.id = id;
		this.fechaTraspaso = fechaTraspaso;
		this.cantidadProdTraspaso = cantidadProdTraspaso;
		this.producto = producto;
		this.bodegaOrigen = bodegaOrigen;
		this.bodegaDestino = bodegaDestino;
		this.usuario = usuario;
	}

	public TraspasoBodega() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getCantidadProdTraspaso() {
		return cantidadProdTraspaso;
	}

	public void setCantidadProdTraspaso(Long cantidadProdTraspaso) {
		this.cantidadProdTraspaso = cantidadProdTraspaso;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getFechaTraspaso() {
		return fechaTraspaso;
	}

	public void setFechaTraspaso(Date fechaTraspaso) {
		this.fechaTraspaso = fechaTraspaso;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Bodega getBodegaOrigen() {
		return bodegaOrigen;
	}

	public void setBodegaOrigen(Bodega bodegaOrigen) {
		this.bodegaOrigen = bodegaOrigen;
	}

	public Bodega getBodegaDestino() {
		return bodegaDestino;
	}

	public void setBodegaDestino(Bodega bodegaDestino) {
		this.bodegaDestino = bodegaDestino;
	}
	
	

}
