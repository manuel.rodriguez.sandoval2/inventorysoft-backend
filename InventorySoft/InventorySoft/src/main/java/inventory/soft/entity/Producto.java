package inventory.soft.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "producto")

public class Producto implements Serializable {

	private static final long serialVersionUID = 1L;

	//@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Id
	@Column(name = "ID_PRODUCTO")
	private Long idProducto;

	@Column(name = "marca_producto")
	private String marcaProducto;

	@Column(name = "nombre_producto")
	private String nombreProducto;

	@Column(name = "unidad_medida")
	private String unidadMedida;

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler" })
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_tipo_producto")
	private TipoProducto tipoProducto;

	/*@JsonIgnoreProperties(value = { "producto", "hibernateLazyInitializer", "handler" }, allowSetters = true)
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "producto", cascade = CascadeType.ALL)
	private List<Ingreso> ingreso;*/

	public Producto() {

	}

	public Producto(Long idProducto, String marcaProducto, String nombreProducto, String unidadMedida,
			TipoProducto tipoProducto) {
		super();
		this.idProducto = idProducto;
		this.marcaProducto = marcaProducto;
		this.nombreProducto = nombreProducto;
		this.unidadMedida = unidadMedida;
		this.tipoProducto = tipoProducto;
	}

	public Producto(Long idProducto, String nombreProducto) {
		super();
		this.idProducto = idProducto;
		this.nombreProducto = nombreProducto;
	}

	public Long getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(Long idProducto) {
		this.idProducto = idProducto;
	}

	public String getMarcaProducto() {
		return marcaProducto;
	}

	public void setMarcaProducto(String marcaProducto) {
		this.marcaProducto = marcaProducto;
	}

	public String getNombreProducto() {
		return nombreProducto;
	}

	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}

	public String getUnidadMedida() {
		return unidadMedida;
	}

	public void setUnidadMedida(String unidadMedida) {
		this.unidadMedida = unidadMedida;
	}

	public TipoProducto getTipoProducto() {
		return tipoProducto;
	}

	public void setTipoProducto(TipoProducto tipoProducto) {
		this.tipoProducto = tipoProducto;
	}

	@Override
	public boolean equals(Object obj) {

		if (!(obj instanceof Producto)) {
			return false;
		}
		Producto p = (Producto) obj;

		return this.idProducto != null && this.idProducto.equals((p.getIdProducto()));

	}

}
