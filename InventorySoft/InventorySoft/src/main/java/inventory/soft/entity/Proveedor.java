package inventory.soft.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "PROVEEDOR")
public class Proveedor {

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name = "id_proveedor")
	private Long id;

	@Column(name = "rut_proveedor")
	private String rut;

	@Column(name = "nombre_proveedor")
	private String nombreProveedor;

	@Column(name = "direccion")
	private String direccion;

	@Column(name = "email")
	private String email;

	@Column(name = "giro")
	private String giro;

	public Proveedor(Long id, String nombreProveedor) {
		super();
		this.id = id;
		this.nombreProveedor = nombreProveedor;
	}

	@JsonIgnoreProperties(value = { "proveedor", "hibernateLazyInitializer", "handler" }, allowSetters = true)
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_comuna")
	private Comuna comunas;

	/*
	 * @JsonIgnoreProperties(value = { "proveedor", "hibernateLazyInitializer",
	 * "handler" }, allowSetters = true)
	 * 
	 * @OneToMany(fetch = FetchType.LAZY, mappedBy = "proveedor", cascade =
	 * CascadeType.ALL) private List<Ingreso> ingreso;
	 */

	public Proveedor(Long id, String rut, String nombreProveedor, String direccion, String email, String giro,
			Comuna comunas) {
		super();
		this.id = id;
		this.rut = rut;
		this.nombreProveedor = nombreProveedor;
		this.direccion = direccion;
		this.email = email;
		this.giro = giro;
		this.comunas = comunas;
	}

	public Proveedor() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public String getNombreProveedor() {
		return nombreProveedor;
	}

	public void setNombreProveedor(String nombreProveedor) {
		this.nombreProveedor = nombreProveedor;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGiro() {
		return giro;
	}

	public void setGiro(String giro) {
		this.giro = giro;
	}

	public Comuna getComunas() {
		return comunas;
	}

	public void setComunas(Comuna comunas) {
		this.comunas = comunas;
	}

}
